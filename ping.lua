
require "constants"

require "collisions"

local vec = require "vec.Vector"
local circle = require "circle"
local entities = require "entities"

local Ping = circle:new{}

function Ping:new(obj)
   obj = obj or {}

   setmetatable(obj, self)
   self.__index = self


   obj.pos = obj.pos or Vector(0, 0)
   obj.r = obj.r or 10
   obj.color = obj.color or {255, 255, 255}
   obj.owner = obj.owner

   obj.maxRadius = obj.maxRadius or PINGRADIUS
   obj.speed = obj.speed or PINGSPEED

   obj.spawnedByInformant = obj.informant or false

   obj.type = "ping"

   return obj
end


function Ping:update(dt)
   self.r = self.r + self.speed * dt
   self.color = {255, 0, 255, 255 - 200 / (self.maxRadius / self.r)} -- TODO: max self radius magic number
--    print(self.color[4])
   if self.r > self.maxRadius then
      entities:Remove(self)
      self = nil
      return
   end

   for _, entity in ipairs(entities) do
      if entity.type == "player" and entity ~= self.owner then
         if CircleCircleCollision(entity.circle, self) then
--            print("asldfkj")
            local additionalTime
            if self.spawnedByInformant then additionalTime = 10
            else additionalTime = 0
            end
            entity.pingedCD:trigger(additionalTime)-- = PINGHIGLIGHTTIME
         end

      elseif entity.type == "informant" then
         if CircleCircleCollision(entity.circle, self) then
            entity.pingedCD:trigger()-- = PINGHIGLIGHTTIME
         end
--      elseif entity.type == "gubbe" then
--         if CircleCircleCollision(entity.circle, self) then
--            entity.pingedCD:trigger()
--         end
      end
   end
end

return Ping
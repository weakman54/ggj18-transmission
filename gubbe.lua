
require "constants"

local Entity = require "entity"
local AnimationCollection = require "animationCollection"
local Circle = require "circle"
local CD = require "cooldownTimer"

local Gubbe = Entity:new()


function Gubbe:new(obj)
   obj = Entity:new(obj)

   setmetatable(obj, self)
   self.__index = self


   obj.animationCollection = obj.animationCollection or AnimationCollection:new()
   obj.movementVector = obj.movementVector or Vector(math.random()*2-1, math.random()*2-1):normalize()
   --print(obj.movementVector)
   obj.speed = obj.speed or GUBBSPEED -- MAGIC NUMBER
   obj.angle = obj.angle or love.math.random(0, 2*math.pi)

   obj.color = {255, 255, 255}

   obj.circle = Circle:new({pos=self.pos, r = 10})

   obj.type = "gubbe"

   obj.pingedCD = CD:new{cooldown = PINGHIGHLIGHTTIME}
   
   return obj
end


function Gubbe:update(dt)
  
   if self.movementVector:len() == 0 then
      --self.animationCollection:setAnimation("still")
      return
   end

   self.animationCollection:setAnimation("moving", true)

   self.pos = self.pos + self.movementVector * self.speed * dt
   self.angle = self.movementVector:angle() + math.pi/2
   
   --self.movementVector:len()
   self.animationCollection.animations["moving"].frameTime = 1 - self.movementVector:len()* WALKANIMATIONSPEEDMULTIPLIER

   self.animationCollection:update(dt)
   if not IsCircleInsideRectangle(self.circle, mapboundsRectangle) then
--      local _, _, _, distEdgeX, distEdgeY = CircleRectCollision(self.circle, mapboundsRectangle)
      
--      self.debugLine = {pos1 = Vector(distEdgeX, distEdgeY) * -10, pos2 = self.pos}
      local angle = self.movementVector:angle();
      angle = angle + math.pi + math.random() * .2 - .1
      local len = self.movementVector:len(); 
      CreateMovementVector(angle, len)
      
      self.movementVector = GenerateRandomMovementVector()
   end
   self.circle.pos = self.pos
   ClampCircleInRectangle(self.circle, mapboundsRectangle)
   self.pos = self.circle.pos

   

   self.pingedCD:update(dt)

   if not self.pingedCD.canUse then
      self.color = {255, 255 * self.pingedCD:percentDone(), 255 * self.pingedCD:percentDone()}
   end
end

function Gubbe:draw()
   self.animationCollection.color = self.color
   self.animationCollection:draw(self.pos.x, self.pos.y, self.angle, 0.125, 0.125, SPRITE_CENTER, SPRITE_CENTER) -- TODO: data
   
   if self.debugLine then
      love.graphics.setColor(255, 0, 255)
      love.graphics.line(self.debugLine.pos1.x, self.debugLine.pos1.y, self.debugLine.pos2.x, self.debugLine.pos2.y)
   end
end

function GenerateRandomMovementVector()
   return Vector(math.random()*2-1, math.random()*2-1):normalize()
end


function CreateMovementVector(angle,length)
  return Vector(length * math.cos(angle), length * math.sin(angle))
  end

return Gubbe
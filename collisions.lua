


function IsCircleInsideRectangle(circle,rectangle)
  if rectangle.pos.x < circle.pos.x-circle.r
  and rectangle.pos.x + rectangle.width > circle.pos.x + circle.r
  and rectangle.pos.y < circle.pos.y - circle.r
  and rectangle.pos.y + rectangle.height > circle.pos.y + circle.r
  then
    return true
  else
    return false
  end
end





function IsPointInsideRectangle(point,rectangle)
  if rectangle.pos.x < point.x
  and rectangle.pos.x + rectangle.width > point.x
  and rectangle.pos.y < point.y
  and rectangle.pos.y + rectangle.height > point.y
  then
    return true
  else
    return false
  end
end


function clamp(min, val, max)
  return math.max(min, math.min(val, max));
end

function ClampPointInRectangle(point, rectangle)
  point.x = clamp(rectangle.pos.x, point.x, rectangle.pos.x + rectangle.width)
  point.y = clamp(rectangle.pos.y, point.y, rectangle.pos.y + rectangle.height)

end

function ClampCircleInRectangle(circle, rectangle)
  circle.pos.x = clamp(rectangle.pos.x + circle.r, circle.pos.x, rectangle.pos.x + rectangle.width - circle.r)
  circle.pos.y = clamp(rectangle.pos.y + circle.r, circle.pos.y, rectangle.pos.y + rectangle.height - circle.r)
end

function ClampPointOutOfRectangle(point, rectangle)
  local a = {
    math.abs(point.x - rectangle.pos.x),
    math.abs(point.x - (rectangle.pos.x + rectangle.width)),
    math.abs(point.y - rectangle.pos.y),
    math.abs(point.y - (rectangle.pos.y + rectangle.height))
  }
  local lowest = 99999
  local lowestIndex = 0
  for i=1, 4 do
    if a[i] < lowest then
      lowest = a[i]
      lowestindex = i
    end
  end
  if lowestindex == 1 then
    point.x = rectangle.pos.x
  end
  if lowestindex == 2 then
    point.x = rectangle.pos.x + rectangle.width
  end
  if lowestindex == 3 then
    point.y = rectangle.pos.y
  end
  if lowestindex == 4 then
    point.y = rectangle.pos.y + rectangle.height
  end
end

function CircleCircleCollision(a, b)
  return (a.r + b.r) > (a.pos - b.pos):len()
end

function CircleRectCollision(circle, rect)
  local inX = clamp(circle.pos.x, rect.pos.x, rect.pos.x + rect.width)
  local inY = clamp(circle.pos.y, rect.pos.y, rect.pos.y + rect.height)
  local dx = circle.pos.x - inX
  local dy = circle.pos.y - inY

  local intersects = (dx * dx + dy * dy) < (circle.r * circle.r)

  return intersects, dx, dy, inX, inY
end
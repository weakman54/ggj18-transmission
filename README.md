
# Covert Transmission

A game by Team Covert, made for (and during) the Global Game Jam 2018


# About 

A quick multiplayer game about blending in with the crowd, detecting the other players, and planning the perfect time to strike. The players take on the roles of spies in a cold-war era setting, using transmitters to find their targets.  
The game is played with at least two gamepads, use the left joystick to move your character. The "X" button will transmit a pulse that detects other players within it's range. The "A" button will make the player slash his knife, killing any player within close range. 

Use the "Back" button to reset the scoreboard. 


# Installation/Build instructions: 

You can start the game by opening the source code FOLDER with the LOVE 2D game engine (Downloadable from: https://love2d.org/), just drag the folder onto the LOVE.exe icon.  


#Platform

Any platform that can run the Love 2D engine (see below)


# Credits

##Game Design: 
Anton Berglund

Jeffrey Chen

Petrut Raileanu

Emil Elthammar

Erik Wallin

##Programming:
Erik Wallin

Emil Elthammar

##Art: 
Jeffrey Chen

Petrut Raileanu

##Post-It note master:
Anton Berglund Sound

##Sound attributions: 
Crowd noice - https://freesound.org/people/unfa/

Soundtrack: Aimbot Blues - Antti Luode https://www.soundclick.com/html5/v3/player.cfm?type=single&songid=13659603&q=hi&newref=1


Repository Link: 
https://bitbucket.org/weakman54/ggj18-transmission.git

local Gubbe = require "gubbe"
local CD = require "cooldownTimer"

local Informant = Gubbe:new()

function Informant:new(obj)
   obj = obj or Gubbe:new(obj)

   setmetatable(obj, self)
   self.__index = self

   obj.type = "informant"

   obj.killed = false

   obj.pingedCD = CD:new{cooldown = PINGHIGHLIGHTTIME}

   return obj
end

function Informant:update(dt)
   
   self.animationCollection:update(dt)
   
   if self.killed then
      self.animationCollection:setAnimation("dying")
      self.pingedCD:reset()
      self.color = {255, 255, 255}
      return
   end
   
   if self.movementVector:len() == 0 then
      --self.animationCollection:setAnimation("still")
      return
   end

   self.animationCollection:setAnimation("moving", true)

   self.pos = self.pos + self.movementVector * self.speed * dt
   self.angle = self.movementVector:angle() + math.pi/2
   
   --self.movementVector:len()
   self.animationCollection.animations["moving"].frameTime = 1 - self.movementVector:len()* WALKANIMATIONSPEEDMULTIPLIER

   if not IsCircleInsideRectangle(self.circle, mapboundsRectangle) then
--      local _, _, _, distEdgeX, distEdgeY = CircleRectCollision(self.circle, mapboundsRectangle)
      
--      self.debugLine = {pos1 = Vector(distEdgeX, distEdgeY) * -10, pos2 = self.pos}
      self.movementVector = GenerateRandomMovementVector()
   end
   ClampPointInRectangle(self.pos, mapboundsRectangle)

   self.circle.pos = self.pos

   self.pingedCD:update(dt)
   
   if not self.pingedCD.canUse then
      self.color = {255 * self.pingedCD:percentDone(), 255, 255 * self.pingedCD:percentDone()}
   end
   
   if self.killed then
      pingedCD:reset()
   end
end


return Informant
local Entities = {}

function Entities:CreateAndAdd(Entity, ...)
	local entity = Entity:new(...)
   table.insert(self, entity)
   return entity
end

function Entities:Remove(entity)
	for i, e in ipairs(self) do
		if entity == e then
			table.remove(self, i)
			return
		end
	end
end


return Entities
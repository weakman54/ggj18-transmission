
require "constants"
require "collisions"

local Entities = require "entities"
local Entity = require "entity"
local AnimationCollection = require "animationCollection"
local Circle = require "circle"
local CDTimer = require "cooldownTimer"

local Player = Entity:new()


function Player:new(obj)
   obj = Entity:new(obj)

   setmetatable(obj, self)
   self.__index = self


   obj.animationCollection = obj.animationCollection or AnimationCollection:new()
   obj.joystickVec = Vector(0, 0)
   obj.speed = obj.speed or PLAYERSPEED
   obj.angle = 0

   obj.color = {255, 255, 255}

--   obj.attackTime = 0
   obj.knifeCD = CDTimer:new{duration = KNIFE_DURATION, cooldown = KNIFE_COOLDOWN}
   obj.type = "player"

   obj.circle = Circle:new{pos = obj.pos, r = KNIFERADIUS, color = {255, 255, 255, 0}}

   obj.pingCD = CDTimer:new{cooldown = PING_COOLDOWN}

--   obj.pingedTime = 0
   obj.pingedCD = CDTimer:new{cooldown = PINGHIGHLIGHTTIME}

   obj.deadCD = CDTimer:new{duration = DEADCOOLDOWN, cooldown = 1}
--  print("please be false", obj.deadCD.canUse)
   obj.killed = false


   obj.score = 0


   return obj
end


function Player:update(dt)
   self.animationCollection:update(dt)

   if self.killed then
      self.deadCD:update(dt);
      if self.deadCD.canUse then
         self:Ressurect();
      end
      return
   end

   if self.knifeCD.happening then
      self.animationCollection:setAnimation("attacking")

      for i, otherplayer in ipairs(players) do -- GLOBAL BULLSHIT
--         print(otherplayer.killed, otherplayer.killed and otherplayer ~= self and CircleCircleCollision(self.circle, otherplayer.circle))
         if otherplayer ~= self and not otherplayer.killed and otherplayer.deadCD.canUse and CircleCircleCollision(self.circle, otherplayer.circle) then
            otherplayer:Die()
            self.score = self.score + PLAYER_KILL_SCORE
         end
      end

      if not informant.killed and CircleCircleCollision(self.circle, informant.circle) then
         informant.killed = true
         self.score = self.score + INFORMANT_KILL_SCORE
         
         CreatePing(self, true)

         love.audio.play("assets/deathscream.mp3") 
     end

   elseif self.joystickVec:len() == 0 then
      self.animationCollection:setAnimation("still")
   else
      self.animationCollection:setAnimation("moving", true)

      self.pos = self.pos + self.joystickVec * self.speed * dt
      self.angle = self.joystickVec:angle() + math.pi/2
   end


   self.animationCollection.animations["moving"].frameTime = 1 - self.joystickVec:len() * WALKANIMATIONSPEEDMULTIPLIER


   ClampPointInRectangle(self.pos, mapboundsRectangle)
   self.circle.pos = self.pos


   self.pingedCD:update(dt)

   if not self.pingedCD.canUse then

      self.color = {255, 255 * self.pingedCD:percentDone(), 255 * self.pingedCD:percentDone()}
   end

--   if self.pingedTime > 0 then
--      print(self.pingedTime)
--      self.color = {255, 255 - 255 / (PINGHIGHLIGHTTIME/self.pingedTime), 255 - 255 / (PINGHIGHLIGHTTIME/self.pingedTime)}
--      self.pingedTime = self.pingedTime - dt
--   else
--      self.color = {255, 255, 255}
--   end


   -- Ping Cooldown
   self.pingCD:update(dt)
   self.knifeCD:update(dt)

   if self.killed then
      self.pingedCD:reset()
      self.pingCD:reset()
      self.knifeCD:reset()
   end
end

function Player:draw()
   if self.killed then

--    self.color = {0, 255, 0, 100}
--    self.circle.color = self.color
   end

   self.animationCollection.color = self.color

--  self.circle:draw()
   self.animationCollection:draw(self.pos.x, self.pos.y, self.angle, 0.125, 0.125, SPRITE_CENTER, SPRITE_CENTER) -- TODO: data MAGIC NUMBERS FOR SCALING AND OFFSET

--  love.graphics.print(self.pingCD:percentDone(), self.pos:unpack())
end


function Player:Die()
   self.killed = true
   self.deadCD:trigger()
   self.animationCollection:setAnimation("dying")
   love.audio.play("assets/deathscream.mp3")
end

function Player:Ressurect()
   self.pos = RandomMapPos()
   self.circle.pos = self.pos

   self.killed = false
   self.color = {255,255,255}
--  self.circle.color = {0,0,0,0}
--  print("woho")
end

return Player
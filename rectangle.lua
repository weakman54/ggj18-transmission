local vec = require "vec.Vector"

local Rectangle = {}

function Rectangle:new(obj)
  obj = obj or {}
  
  setmetatable(obj, self)
  self.__index = self
  
  
  obj.pos = obj.pos or Vector(0, 0)
  obj.width = obj.width or 1
  obj.height = obj.height or 1
  obj.color = obj.color or {255, 255, 255}
  
  obj.type = "rectangle"
  
  return obj
end

function Rectangle:update(dt)
   -- Empty for now
end

function Rectangle:draw()
  love.graphics.setColor(self.color)
  love.graphics.rectangle("line", self.pos.x, self.pos.y, self.width, self.height)
end


function Rectangle:collidingWith(other)
  return (self.r + other.r) > (self.pos - other.pos):len()
end



return Rectangle
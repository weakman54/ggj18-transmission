


--function newAnimation(image, width, height, duration)
--    local animation = {}
--    animation.spriteSheet = image;
--    animation.quads = {};


--end



local Animation = {}

function Animation:new(image, width, height, frameTime, looping)
   local obj = {}

   if type(image) == "string" then
      image = love.graphics.newImage(image)
   end

   setmetatable(obj, self)
   self.__index = self

   obj.texture = image
   obj.quads = {}


   for y = 0, image:getHeight() - height, height do
      for x = 0, image:getWidth() - width, width do
         table.insert(obj.quads, love.graphics.newQuad(x, y, width, height, image:getDimensions()))
      end
   end

   obj.frameTime = frameTime or 0.1
   obj.curTime = 0

   obj.curFrameI = 1
   obj.numFrames = #obj.quads
   obj.looping = looping or false
   obj.playing = true

   obj.color = {255, 255, 255}

   obj.centerOffset = Vector(width/2, height/2)

   return obj
end


function Animation:update(dt)
   if not self.playing then return end

   self.curTime = self.curTime + dt

   if self.curTime >= self.frameTime then
      self:increment()
      self.curTime = self.curTime - self.frameTime
   end
end

function Animation:increment()
   self:setFrameI(self.curFrameI + 1)
end
function Animation:setFrameI(i)
   if i > self.numFrames and not self.looping then
      self.curFrameI = self.numFrames -- TODO: This needs more testing
      self.playing = false
   else
      self.curFrameI = ((i-1) % self.numFrames) + 1
   end
end


function Animation:draw(...)
   love.graphics.setColor(self.color)
   love.graphics.draw(self.texture, self.quads[self.curFrameI], ...) -- MAGIC NUMBER: scale value
end


function Animation:play(loop)
   self.playing = true
   self.looping = loop or false -- Redundant, but clearer

   return self -- convenience, can probably remove later
end

function Animation:pause()
   self.playing = false
end

function Animation:stop()
   self.playing = false
   self:setFrameI(1)
end


return Animation

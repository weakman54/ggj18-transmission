
require "vec.Vector"
local Entity = {}

function Entity:new(obj)
   obj = obj or {}
   
   setmetatable(obj, self)
   self.__index = self
   
   obj.pos = obj.pos or Vector()
   obj.type = "generic"
   
   return obj
end

function Entity:draw()
   -- placeholder
end

function Entity:update(dt)
   -- placeholder
end



return Entity
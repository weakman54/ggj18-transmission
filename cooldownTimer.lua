
local CD = {}

function CD:new(obj)
   obj = obj or {}

   setmetatable(obj, self)
   self.__index = self

   obj.timer = 0
   obj.duration = obj.duration or 0
   obj.cooldown = obj.cooldown or 0

   obj.canUse = obj.canUse or true
   obj.happening = false
   return obj
end

function CD:reset()
   self.timer = 0
   self.canUse = true
   self.happening = false
end

function CD:trigger(additionalTime)
   additionalTime = additionalTime or 0
   self.timer = -additionalTime
   self.canUse = false
   self.happening = true
end

function CD:update(dt)
   if not self.canUse then
      self.timer = self.timer + dt

      if self.timer < self.duration then
         self.happening = true
         return
      end

      self.happening = false

      if self.timer > (self.duration + self.cooldown) then
         self.canUse = true
         self.happening = false
         self.timer = 0
         return
      end
   end
end


function CD:percentDone()
   if self.canUse then
      return 1
   end

   return self.timer / (self.cooldown - self.duration)
end

function CD:invPercentDone()
   return 1 - self:percentDone()
end



return CD
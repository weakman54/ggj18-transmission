
--require "vec.Vector"
local Entity = require "entity"

local Circle = Entity:new()

function Circle:new(obj)
  obj = Entity:new(obj)
  
  setmetatable(obj, self)
  self.__index = self
  
  
  obj.r = obj.r or 10
  obj.color = obj.color or {255, 255, 255}
  
  obj.type = "circle"
  
  return obj
end

function Circle:draw()
  love.graphics.setColor(self.color)
  love.graphics.circle("line", self.pos.x, self.pos.y, self.r)
end



return Circle